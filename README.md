Using React-Native-Mapps 
========================


Setup
-----

Step 1

First set up with react-native.


Step 2

Next, create a new react-native project.



I prefer the JetBrain WebStorm because
 * it seems to do better than Visual Studio Code in terms of finding code usage and
 * It automatically sets up the run and debug profile from the toolbar so that the separate metro process does not run outside of WebStorm.  (What am I talking about?  )

Of if you do it from the command line:

react-native init rnmapdemo
 

Step 3

Add the react-native-maps to the project

From the command line inside the project:

cd ~/rnmapdemo
npm install react-native-maps --save

Since at the time of this writing the link is now done automatically, you don't need to do this :

react-native link react-native-maps


Step 4

Setting up for Android 

Step 4.1
Setting the API Key and enabling Google Map
With your Google account, go to this link:

https://console.cloud.google.com/home/dashboard

Create a new project or select an existing project

![Select or New Project](docs/images/newproject.png)

Next create the API key by going to the hamburger menu and going to API & Services and then Credentials.

![Credentials](docs/images/APIServicesCredentials.png)




You 

Step 5

Setting up for iOS





based on https://codeburst.io/react-native-google-map-with-react-native-maps-572e3d3eee14

https://github.com/react-native-community/react-native-maps/blob/master/docs/installation.md

https://github.com/anhtuank7c/maps-example

